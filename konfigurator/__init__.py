import re
import shutil
import subprocess
import sys
import tempfile
from pathlib import Path

tmp_dir = Path(tempfile.mkdtemp())


class User:
    def __init__(self, name, home):
        self.name = name
        self.home = Path(home)


def run_live_process(cmd):
    subprocess.Popen(cmd, stdout=sys.stdout).wait()


def users() -> list[User]:
    uid_min_re = re.compile(r"^UID_MIN\s+(\d+)$", re.MULTILINE)
    uid_max_re = re.compile(r"^UID_MAX\s+(\d+)$", re.MULTILINE)

    min_uid, max_uid = -1, -1

    with open("/etc/login.defs", 'r') as f:
        for line in f:
            min_match = uid_min_re.match(line)
            if min_match:
                min_uid = int(min_match.groups()[0])

            max_match = uid_max_re.match(line)
            if max_match:
                max_uid = int(max_match.groups()[0])

            if min_uid != -1 and max_uid != -1:
                break

    if min_uid == -1 or max_uid == -1:
        raise RuntimeError("Failed to parse login.defs.")

    user_re = re.compile(r"^(?P<user>[^:]+):[^:]*:(?P<uid>\d+)(?::[^:]*){2}:(?P<home>[^:]+):[^:]*$",
                         re.MULTILINE)

    user_list = []

    with open("/etc/passwd", 'r') as f:
        for line in f:
            user_match = user_re.match(line)

            if user_match:
                user_uid = int(user_match.group("uid"))

                if user_uid == 0 or min_uid <= user_uid <= max_uid:
                    user_list.append(User(user_match.group("user"),
                                          user_match.group("home")))

    return user_list


def clean_install():
    print("\nRemoving packets...\n")
    run_live_process(["apt-get", "-y", "purge", "kmail", "korganizer", "kaddressbook", "kamera",
                      "apper", "akregator", "dragonplayer", "k3b", "juk", "konqueror", "termit",
                      "kfind", "kmouth", "sweeper", "kmousetool", "knotes", "kwrite", "kontrast",
                      "kmag"])

    print("\nPurging...\n")
    run_live_process(["apt-get", "-y", "--purge", "autoremove"])

    print("\nCleaning...\n")
    run_live_process(["apt-get", "-y", "clean"])
    run_live_process(["apt-get", "-y", "autoclean"])

    print("\nDone.\n")


def install_geogebra():
    geogebra_keyring = Path("/usr/local/share/keyrings")
    tmp_key = tmp_dir / "tmp.key"

    print("\nImporting Geogebra Key...\n")

    run_live_process(["wget", "https://static.geogebra.org/linux/office@geogebra.org.gpg.key",
                      "-O", str(tmp_key)])

    run_live_process(["gpg", "--no-default-keyring", "--homedir", str(tmp_dir),
                      "--import", str(tmp_key)])

    run_live_process(["gpg", "--no-default-keyring", "--homedir", str(tmp_dir),
                      "--export", "--output", str(geogebra_keyring)])

    print("\nInstalling Geogebra Repository...\n")

    with open("/etc/apt/sources.list.d/geogebra.sources", "w") as f:
        f.write("Types: deb\n"
                "URIs: http://www.geogebra.net/linux/\n"
                "Suites: stable\n"
                "Components: main\n"
                f"Signed-By: {str(geogebra_keyring)}\n")

    run_live_process(["apt-get", "-y", "update"])

    print("\nInstalling Geogebra...\n")
    run_live_process(["apt-get", "-y", "install", "geogebra"])

    print("\nDone.\n")


def install_minecraft():
    minecraft_deb = tmp_dir / "minecraft.deb"

    print("\nDownloading Minecraft Deb...\n")
    run_live_process(["wget", "https://launcher.mojang.com/download/Minecraft.deb",
                      "-O", str(minecraft_deb)])

    print("\nInstalling Minecraft Package...\n")
    run_live_process(["apt-get", "-y", "install", str(minecraft_deb)])


def install_flatpaks():
    print("\nAdding Flathub Repo...\n")
    run_live_process(["flatpak", "remote-add", "--if-not-exists", "flathub",
                      "https://flathub.org/repo/flathub.flatpakrepo"])

    print("\nInstalling Spotify...\n")
    run_live_process(["flatpak", "install", "-y", "flathub", "com.spotify.Client"])

    print("\nInstalling Citra...\n")
    run_live_process(["flatpak", "install", "-y", "flathub", "org.citra_emu.citra"])


def update_packages():
    print("\nUpdating and Upgrading Distro...\n")
    run_live_process(["apt-get", "-y", "update"])
    run_live_process(["apt-get", "-y", "upgrade"])
    run_live_process(["apt-get", "-y", "dist-upgrade"])

    # firewall ?

    print("\nInstalling Packages...\n")
    run_live_process(["apt-get", "-y", "install", "sddm-theme-debian-breeze", "unattended-upgrades",
                      "libreoffice", "pdfshuffler", "default-jdk", "inkscape", "simple-scan",
                      "pdfmod", "hplip", "hplip-gui", "gparted", "plasma-discover-backend-flatpak",
                      "flatpak", "net-tools", "htop", "vlc", "dolphin-nextcloud", "openssh-server",
                      "gimp", "kile", "auto-updater", "auto-stopper", "curl"])

    print("\nDone.\n")


def clean_and_exit():
    print("\nCleaning...\n")
    run_live_process(["apt-get", "-y", "update"])
    run_live_process(["apt-get", "-y", "upgrade"])
    run_live_process(["apt-get", "-y", "autoremove", "--purge"])
    run_live_process(["apt-get", "-y", "clean"])
    run_live_process(["apt-get", "-y", "autoclean"])

    shutil.rmtree(tmp_dir)
    print("\nDone.\n")


def bashrc_update(user: User):
    if user.name == "root":
        (user.home / ".bashrc").unlink()
        shutil.copy(Path("/etc/skel/.bashrc"), user.home / ".bashrc")

    subprocess.check_call(["sed", "-i", r"s/#alias/alias/g", user.home / ".bashrc"])
    subprocess.check_call(["sed", "-i", r"s/#export/export/g", user.home / ".bashrc"])
    subprocess.check_call(["sed", "-i", r"s/#force_color_prompt/force_color_prompt/g",
                           user.home / ".bashrc"])

    subprocess.check_call(["sed", "-i", r"s/ls -l/ls -al/g", user.home / ".bashrc"])
    if user.name == "root":
        with open(user.home / ".bashrc", 'a') as f:
            f.write("\n")
            f.write(r"PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[00m\]@"
                    r"\[\033[01;32m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '")
            f.write("\n")
    else:
        with open(user.home / ".bashrc", 'a') as f:
            f.write("\n")
            f.write(r"PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]@"
                    r"\[\033[01;32m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '")
            f.write("\n")

    print("\nDone.\n")


def main():
    clean_install()
    update_packages()
    install_geogebra()
    install_minecraft()
    install_flatpaks()

    for user in users():
        bashrc_update(user)

    clean_and_exit()
