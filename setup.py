from setuptools import setup

setup(
    name="desfrene-konfigurator",
    version="1.0",
    platforms="any",
    url="https://gitlab.com/desfrene/desfrene-konfigurator",
    license="GPL-3",
    packages=["konfigurator"],
    author="Gabriel Desfrene",
    author_email="gabriel@desfrene.fr",
    description="Setup Desfrene Computers.",
    entry_points={
        "console_scripts": [
            "konfigurator=konfigurator:main",
        ]
    },
    python_requires=">=3.9",
)
